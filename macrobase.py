from ahk import AHK
from pprint import pprint


class MacroBase():

	def __init__(self):
		self.name = 'MacroBase AHK Companion'
		self.ahk = AHK()
		self.hotkeys_active = False

	# Hotkeys without modifiers
	def f13(self):
		pass

	def f14(self):
		pass

	def f15(self):
		pass

	def f16(self):
		pass

	def f17(self):
		pass

	def f18(self):
		pass

	def f19(self):
		pass

	def f20(self):
		pass

	def f21(self):
		pass

	def f22(self):
		pass

	def f23(self):
		pass

	def f24(self):
		self.ahk.type('f24')

	# Hotkeys with LEFT ALT modifier
	def alt_f13(self):
		pass

	def alt_f14(self):
		pass

	def alt_f15(self):
		pass

	def alt_f16(self):
		pass

	def alt_f17(self):
		pass

	def alt_f18(self):
		pass

	def alt_f19(self):
		pass

	def alt_f20(self):
		pass

	def alt_f21(self):
		pass

	def alt_f22(self):
		pass

	def alt_f23(self):
		pass

	def alt_f24(self):
		pass

	# Hotkeys with LEFT ALT modifier
	def shift_f13(self):
		pass

	def shift_f14(self):
		pass

	def shift_f15(self):
		pass

	def shift_f16(self):
		pass

	def shift_f17(self):
		pass

	def shift_f18(self):
		pass

	def shift_f19(self):
		pass

	def shift_f20(self):
		pass

	def shift_f21(self):
		pass

	def shift_f22(self):
		pass

	def shift_f23(self):
		pass

	def shift_f24(self):
		pass

	# Hotkeys with LEFT ALT modifier
	def ctrl_f13(self):
		self.ahk.type('ctrl_f13')

	def ctrl_f14(self):
		pass

	def ctrl_f15(self):
		pass

	def ctrl_f16(self):
		pass

	def ctrl_f17(self):
		pass

	def ctrl_f18(self):
		pass

	def ctrl_f19(self):
		pass

	def ctrl_f20(self):
		pass

	def ctrl_f21(self):
		pass

	def ctrl_f22(self):
		pass

	def ctrl_f23(self):
		pass

	def ctrl_f24(self):
		pass

	def exception_handler(hotkey: str, exception: Exception):
		print('exception with callback for hotkey', hotkey, 'Here was the error:', exception)

	# hotkey handling
	def hotkeys_enable(self):
		if not self.hotkeys_active:
			self.ahk.add_hotkey('^F13', callback=self.ctrl_f13, ex_handler=self.exception_handler)
			self.ahk.add_hotkey('F24', callback=self.ctrl_f13, ex_handler=self.exception_handler)
			pprint('enable hotkeys')
			self.hotkeys_active = True

	def hotkeys_disable(self):
		if self.hotkeys_active:
			# remove single hotkey
			self.ahk.remove_hotkey('^F13')
			self.ahk.remove_hotkey('F24')
			# remove all hotkeys
			self.ahk.clear_hotkeys()
			pprint('disable hotkeys')
			self.hotkeys_active = False
	
	def main(self):
		pprint('main')
		while True:
			if not self.hotkeys_active:
				self.hotkeys_enable()


if __name__ == '__main__':
	mb = MacroBase()
	mb.main()