#Requires AutoHotkey v2.0
; https://github.com/qmk/qmk_firmware/blob/master/keyboards/1upkeyboards/super16/keymaps/ahk_companion/ahk_companion.ahk
; https://github.com/zauberwild/Pico-Macropad/blob/v1.0/pico-macropad.ahk
; https://stackoverflow.com/questions/25414617/combination-of-specific-key-and-any-other-key
; #NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode("Input")  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir(A_ScriptDir)  ; Ensures a consistent starting directory.
#Singleinstance force
;SetFormat, float, 0
Persistent
SetTitleMatchMode 1
InstallKeybdHook
InstallMouseHook
#MaxThreadsPerHotkey 16

; check if we run with admin privileges
full_command_line := DllCall("GetCommandLine", "str")

if not (A_IsAdmin or RegExMatch(full_command_line, " /restart(?!\S)"))
{
    try
    {
        if A_IsCompiled
            Run '*RunAs "' A_ScriptFullPath '" /restart'
        else
            Run '*RunAs "' A_AhkPath '" /restart "' A_ScriptFullPath '"'
    }
    ExitApp
}

#Include "libs/shared.ahk"
#Include "libs/keys.ahk"

input := "149#420"
window_test_title := "PrusaSlicer"

y_toggle := True
spot_toggle := False
clicker_toggle := False

;=================================================================
;                      Macro Pad Functions                       ;
;=================================================================
;;; Date/Time Methods
CurrentDate() {
	dateStamp := FormatTime( , "MM.dd.yyyy")
    Send %dateStamp%
}

CurrentTime() {
	timeStamp := FormatTime( , "HH:mm:ss")
    Send %timeStamp%
}

; Google bang
; https://github.com/Gateway3B/G3-Macros/blob/master/AHK/HotKeyFunctions.ahk


; suspend script
F24::
{
	suspend -1
	return
}


<^+!LWin::
{
	ih := InputHook("L8", ".")
	ih.Start()
	ih.Wait()

	; suspend script
	if(ih.Input == "suspend") {
		suspend -1
		return
	}

	if(ih.Input == "a") {
		MsgBox "A pressed"
	}

	if(ih.Input == "clicker") {
		global clicker_toggle
		clicker_toggle := !clicker_toggle
		Loop
		{
			If (!clicker_toggle)
				Break
			Click
			Sleep 64 ; Make this number higher for slower clicks, lower for faster.
		}
	}


	return
}


; START BF1
; ahk_class Battlefield™ 1
; ahk_exe bf1.exe
#Hotif WinActive("ahk_exe bf1.exe")
; disable console key
^::return 

; disable left win key
LWin::return

; prone (Y) when reloading (R)
~r::
{
	global y_toggle

	if y_toggle {
		Sendinput "{y}"
		if y_toggle AND GetKeyState("R") {
			y_toggle := !y_toggle
		}
	}
	return
}

~y::
{
	global y_toggle

	if not y_toggle {
		y_toggle := !y_toggle
	}
	return
}

; start auto spotter
$*F23::
{
	global spot_toggle
	spot_toggle := !spot_toggle
	loop
	{
		If not spot_toggle
			break
		sendinput "{Blind}{WheelUp}"
		Sleep 100
	}
	return
}

; END BF1