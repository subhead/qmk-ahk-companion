#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.


F12::
{
	Input, userinput, , .
	MsgBox %userinput%
	Pos := 1
	While Pos := RegExMatch(userinput, "(\d+)", Match, Pos + StrLen(Match)) {
		Variable%A_Index% := Match
	}
	Result =
	(LTrim
		Variable 1 = %Variable1%
		Variable 2 = %Variable2%
	)

	layer := % Variable1
	btn := % Variable2

	MsgBox Layer %layer% Btn %btn%
	return
}